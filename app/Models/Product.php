<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $primaryKey = 'product_id';
    protected $guarded = ['product_id'];

        /*Relation*/
    public function orderItems()
    {
        return $this->belongsToMany(Order::class , 'order_product' ,
            'product_id' , 'order_id');
    }

    public function option()
    {
        return $this->belongsTo(Option::class , 'option_id');
    }
        /*End Relation*/
}
