<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = 'order_id';
    protected $guarded = ['order_id'];
    
    /*Relation*/
    public function Products()
    {
        return $this->belongsToMany(Product::class , 'order_product' ,
            'order_id' , 'product_id' );
   }

    public function user()
    {
        return $this->belongsTo(User::class , 'user_id' );
    }

    public function options()
    {
        return $this->hasMany(Option::class , 'option_id');
    }
}
