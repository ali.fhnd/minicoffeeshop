<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $primaryKey ='option_id';
    protected $guarded = ['option_id'];

    /*Relation*/
    public function order()
    {
        return $this->belongsTo(Order::class , 'option_id');
    }

    public function option_values()
    {
        return $this->hasMany(OptionValues::class , 'option_id');
    }

    public function product()
    {
        return $this->hasMany(Product::class , 'option_id');
    }
    /*End Relation*/
}
