<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OptionValues extends Model
{
    protected $primaryKey = 'option_value_id';
    protected $guarded = ['option_value_id'];

        /*Relation*/
    public function option()
    {
        return $this->belongsTo(Option::class , 'option_id');
    }

    /*End Relation*/
}
