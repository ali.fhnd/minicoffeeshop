<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::get();
        return view('' , compact('products'));
    }

    public function create()
    {
        $products = Product::pluck('name','description','product_id')->toArray();
        return view('' , compact('products'));
    }

    public function store(Request $request)
    {
        $newProduct = Product::create([
            'name' => $request->input(),
            'description' =>$request->input(),
            'price'=>$request->input()
        ]);
        if ($newProduct && $newProduct instanceof Product){
            return redirect()->route()->with('status' , 'محصول جدید با موفقیت اضافه شد.');
        }
    }

    public function edit(Request $request , $product_id)
    {
        $item = Product::find($product_id);
        $product = Product::pluck('name','description','product_id')->toArray();
        unset($product[$product_id]);
        return view('' , compact('item' , 'product'));
    }

    public function update(Request $request, $product_id)
    {
        $product = Product::find($product_id);
        $product->update([
           'name'=> $request->input(''),
           'description' => $request->input(''),
        ]);
        if (is_a($product,Product::class)){
            return redirect()->route('')->with('status' , 'محصول با موفقیت بروزرسانی گردید.');
        }
    }

    public function delete(Request $request , $product_id)
    {
        Product::destroy($product_id);
        return back()->with('status' , 'محصول با موفقیت حذف شد.');
    }
}
